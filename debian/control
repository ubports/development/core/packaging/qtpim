Source: qtpim-opensource-src
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Debian UBports Team <team+ubports@tracker.debian.org>,
           Timo Jyrinki <timo@debian.org>,
           Mike Gabriel <sunweaver@debian.org>,
Build-Depends: dbus,
               debhelper-compat (= 13),
               pkg-kde-tools,
               qml-module-qtquick2,
               qml-module-qttest,
               qtbase5-private-dev,
               qtdeclarative5-private-dev,
               qttools5-dev-tools,
               rdfind,
               symlinks,
               xauth <!nocheck>,
               xvfb <!nocheck>,
Build-Depends-Indep: qdoc-qt5,
                     qtbase5-doc-html,
                     qtdeclarative5-doc-html,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://www.qt.io/developers
Vcs-Git: https://salsa.debian.org/qt-kde-team/qt/qtpim.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/qt/qtpim/

Package: libqt5contacts5a
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
Breaks: libqt5contacts5,
Replaces: libqt5contacts5,
Description: Qt PIM module, Contacts library
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains Qt PIM module's Contacts library.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.

Package: qml-module-qtcontacts
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: libqt5contacts5a,
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: qtdeclarative5-qtcontacts-plugin (<< 5.0~git20140515~29475884-0ubuntu3~),
Replaces: qtdeclarative5-qtcontacts-plugin (<< 5.0~git20140515~29475884-0ubuntu3~),
Description: Qt 5 Contacts QML module
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the Qt Contacts QML module for Qt Declarative.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.

Package: libqt5organizer5a
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
Breaks: libqt5organizer5,
Replaces: libqt5organizer5,
Description: Qt PIM module, Organizer library
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains Qt PIM module's Organizer library.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.

Package: qml-module-qtorganizer
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: libqt5organizer5a,
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: qtdeclarative5-qtorganizer-plugin (<< 5.0~git20140515~29475884-0ubuntu3~),
Replaces: qtdeclarative5-qtorganizer-plugin (<< 5.0~git20140515~29475884-0ubuntu3~),
Description: Qt 5 Organizer QML module
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the Qt Organizer QML module for Qt Declarative.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.

Package: libqt5versit5a
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
Breaks: libqt5versit5,
Replaces: libqt5versit5,
Description: Qt PIM module, Versit library
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains Qt PIM module's Versit library.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.

Package: libqt5versitorganizer5a
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
Breaks: libqt5versitorganizer5,
Replaces: libqt5versitorganizer5,
Description: Qt PIM module, Versit Organizer library
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains Qt PIM module's Versit Organizer library.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.

Package: qtpim5-dev
Section: libdevel
Architecture: any
Depends: libqt5contacts5a (= ${binary:Version}),
         libqt5organizer5a (= ${binary:Version}),
         libqt5versit5a (= ${binary:Version}),
         libqt5versitorganizer5a (= ${binary:Version}),
         ${misc:Depends},
Multi-Arch: same
Description: Qt 5 PIM development files
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the header development files used for building Qt 5
 applications using Qt PIM libraries.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.

Package: qtpim5-private-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: qtpim5-dev (= ${binary:Version}),
         ${misc:Depends}
Description: Qt 5 PIM private development files
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the private header development files used for
 building Qt 5 applications using Qt PIM libraries.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.


Package: qtpim5-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Qt 5 PIM documentation
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the documentation for the Qt 5 PIM
 module, including Contacts, Organizer and Versit.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.

Package: qtpim5-doc-html
Architecture: all
Section: doc
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Qt 5 PIM HTML documentation
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the HTML documentation for the Qt 5 PIM
 module, including Contacts, Organizer and Versit.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.


Package: qtpim5-examples
Architecture: any
Section: libdevel
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
Multi-Arch: same
Description: Qt 5 PIM HTML examples
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the examples for Qt 5 PIM.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.
