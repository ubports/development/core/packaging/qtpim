From 7b1c8ff1b9e1ddc7c41a20b6bb306383b3f77734 Mon Sep 17 00:00:00 2001
From: Chris Adams <chris.adams@qinetic.com.au>
Date: Thu, 1 Oct 2020 17:08:38 +1000
Subject: [PATCH 15/32] Remove usage of deprecated API from the declarative
 plugins

Change-Id: Id2f9f936faf687ed51ab9adfca915711f1e65b9b
Reviewed-by: Pekka Vuorela <pvuorela@iki.fi>
Signed-off-by: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
---
 src/imports/contacts/plugin.cpp               |  3 +--
 .../contacts/qdeclarativecontactmodel.cpp     | 12 ++++++-----
 .../contacts/qdeclarativecontactmodel_p.h     |  2 ++
 .../qdeclarativecontactrelationshipmodel.cpp  | 20 +++++++++++--------
 .../qdeclarativecontactrelationshipmodel_p.h  |  2 +-
 src/imports/organizer/plugin.cpp              |  2 +-
 .../organizer/qdeclarativeorganizermodel.cpp  | 12 ++++++-----
 .../organizer/qdeclarativeorganizermodel_p.h  |  2 ++
 8 files changed, 33 insertions(+), 22 deletions(-)

diff --git a/src/imports/contacts/plugin.cpp b/src/imports/contacts/plugin.cpp
index d6745ad0..c30e9548 100644
--- a/src/imports/contacts/plugin.cpp
+++ b/src/imports/contacts/plugin.cpp
@@ -124,8 +124,7 @@ public:
         qmlRegisterType<QDeclarativeContactIntersectionFilter>(uri, major, minor, "IntersectionFilter");
         qmlRegisterType<QDeclarativeContactUnionFilter>(uri, major, minor, "UnionFilter");
         qmlRegisterType<QDeclarativeContactInvalidFilter>(uri, major, minor, "InvalidFilter");
-        qmlRegisterType<QDeclarativeContactCompoundFilter>();
-
+        qmlRegisterType<QDeclarativeContactCompoundFilter>(uri, major, minor, "CompoundFilter");
     }
 
     void initializeEngine(QQmlEngine *engine, const char *uri)
diff --git a/src/imports/contacts/qdeclarativecontactmodel.cpp b/src/imports/contacts/qdeclarativecontactmodel.cpp
index 27e331dc..24872d86 100644
--- a/src/imports/contacts/qdeclarativecontactmodel.cpp
+++ b/src/imports/contacts/qdeclarativecontactmodel.cpp
@@ -225,11 +225,6 @@ QDeclarativeContactModel::QDeclarativeContactModel(QObject *parent) :
     QAbstractListModel(parent),
     d(new QDeclarativeContactModelPrivate)
 {
-    QHash<int, QByteArray> roleNames;
-    roleNames = QAbstractItemModel::roleNames();
-    roleNames.insert(ContactRole, "contact");
-    setRoleNames(roleNames);
-
     connect(this, SIGNAL(managerChanged()), SLOT(doUpdate()));
     connect(this, SIGNAL(filterChanged()), SLOT(doContactUpdate()));
     connect(this, SIGNAL(fetchHintChanged()), SLOT(doContactUpdate()));
@@ -244,6 +239,13 @@ QDeclarativeContactModel::~QDeclarativeContactModel()
 {
 }
 
+QHash<int, QByteArray> QDeclarativeContactModel::roleNames() const
+{
+    QHash<int, QByteArray> roleNames = QAbstractItemModel::roleNames();
+    roleNames.insert(ContactRole, "contact");
+    return roleNames;
+}
+
 /*!
   \qmlproperty string ContactModel::manager
 
diff --git a/src/imports/contacts/qdeclarativecontactmodel_p.h b/src/imports/contacts/qdeclarativecontactmodel_p.h
index 68df46aa..085974e5 100644
--- a/src/imports/contacts/qdeclarativecontactmodel_p.h
+++ b/src/imports/contacts/qdeclarativecontactmodel_p.h
@@ -103,6 +103,8 @@ public:
         ImportParseError       = QVersitReader::ParseError
     };
 
+    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
+
     QString manager() const;
     void setManager(const QString& manager);
 
diff --git a/src/imports/contacts/qdeclarativecontactrelationshipmodel.cpp b/src/imports/contacts/qdeclarativecontactrelationshipmodel.cpp
index ff4e5a77..a9a1aad3 100644
--- a/src/imports/contacts/qdeclarativecontactrelationshipmodel.cpp
+++ b/src/imports/contacts/qdeclarativecontactrelationshipmodel.cpp
@@ -100,10 +100,6 @@ QDeclarativeContactRelationshipModel::QDeclarativeContactRelationshipModel(QObje
     : QAbstractListModel(parent)
     , d(new QDeclarativeContactRelationshipModelPrivate)
 {
-    QHash<int, QByteArray> roleNames;
-    roleNames = QAbstractItemModel::roleNames();
-    roleNames.insert(RelationshipRole, "relationship");
-    setRoleNames(roleNames);
     connect(this, SIGNAL(managerChanged()), SLOT(fetchAgain()));
     connect(this, SIGNAL(participantChanged()), SLOT(fetchAgain()));
     connect(this, SIGNAL(relationshipTypeChanged()), SLOT(fetchAgain()));
@@ -115,6 +111,13 @@ QDeclarativeContactRelationshipModel::~QDeclarativeContactRelationshipModel()
     delete d;
 }
 
+QHash<int, QByteArray> QDeclarativeContactRelationshipModel::roleNames() const
+{
+    QHash<int, QByteArray> roleNames = QAbstractItemModel::roleNames();
+    roleNames.insert(RelationshipRole, "relationship");
+    return roleNames;
+}
+
 /*!
   \qmlproperty string RelationshipModel::manager
 
@@ -265,7 +268,7 @@ void QDeclarativeContactRelationshipModel::setAutoUpdate(bool autoUpdate)
   */
 QQmlListProperty<QDeclarativeContactRelationship> QDeclarativeContactRelationshipModel::relationships()
 {
-    return QQmlListProperty<QDeclarativeContactRelationship>(this, d->m_declarativeRelationships);
+    return QQmlListProperty<QDeclarativeContactRelationship>(this, &d->m_declarativeRelationships);
 }
 
 int QDeclarativeContactRelationshipModel::rowCount(const QModelIndex &parent) const
@@ -343,15 +346,15 @@ void QDeclarativeContactRelationshipModel::requestUpdated()
 
         QList<QContactRelationship> relationships = req->relationships();
 
-        reset();
-        beginInsertRows(QModelIndex(), 0, relationships.count());
-
+        beginResetModel();
         foreach(QDeclarativeContactRelationship* dcr, d->m_declarativeRelationships) {
             dcr->deleteLater();
         }
         d->m_declarativeRelationships.clear();
         d->m_relationships.clear();
+        endResetModel();
 
+        beginInsertRows(QModelIndex(), 0, relationships.count());
         foreach (const QContactRelationship& cr, relationships) {
             QDeclarativeContactRelationship* dcr = new QDeclarativeContactRelationship(this);
             dcr->setRelationship(cr);
@@ -359,6 +362,7 @@ void QDeclarativeContactRelationshipModel::requestUpdated()
             d->m_relationships.append(cr);
         }
         endInsertRows();
+
         req->deleteLater();
         emit relationshipsChanged();
     }
diff --git a/src/imports/contacts/qdeclarativecontactrelationshipmodel_p.h b/src/imports/contacts/qdeclarativecontactrelationshipmodel_p.h
index 8688be52..8fe470f2 100644
--- a/src/imports/contacts/qdeclarativecontactrelationshipmodel_p.h
+++ b/src/imports/contacts/qdeclarativecontactrelationshipmodel_p.h
@@ -67,8 +67,8 @@ public:
     ~QDeclarativeContactRelationshipModel();
     enum {
         RelationshipRole = Qt::UserRole + 500
-
     };
+    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
 
     QString manager() const;
     void setManager(const QString& manager);
diff --git a/src/imports/organizer/plugin.cpp b/src/imports/organizer/plugin.cpp
index 66941156..971740bf 100644
--- a/src/imports/organizer/plugin.cpp
+++ b/src/imports/organizer/plugin.cpp
@@ -123,7 +123,7 @@ public:
         qmlRegisterType<QDeclarativeOrganizerItemIntersectionFilter>(uri, major, minor, "IntersectionFilter");
         qmlRegisterType<QDeclarativeOrganizerItemUnionFilter>(uri, major, minor, "UnionFilter");
         qmlRegisterType<QDeclarativeOrganizerItemInvalidFilter>(uri, major, minor, "InvalidFilter");
-        qmlRegisterType<QDeclarativeOrganizerItemCompoundFilter>();
+        qmlRegisterType<QDeclarativeOrganizerItemCompoundFilter>(uri, major, minor, "CompoundFilter");
     }
 };
 
diff --git a/src/imports/organizer/qdeclarativeorganizermodel.cpp b/src/imports/organizer/qdeclarativeorganizermodel.cpp
index c191f37a..b1420e0b 100644
--- a/src/imports/organizer/qdeclarativeorganizermodel.cpp
+++ b/src/imports/organizer/qdeclarativeorganizermodel.cpp
@@ -198,11 +198,6 @@ QDeclarativeOrganizerModel::QDeclarativeOrganizerModel(QObject *parent) :
     QAbstractListModel(parent),
     d_ptr(new QDeclarativeOrganizerModelPrivate)
 {
-    QHash<int, QByteArray> roleNames;
-    roleNames = QAbstractItemModel::roleNames();
-    roleNames.insert(OrganizerItemRole, "item");
-    setRoleNames(roleNames);
-
     d_ptr->m_updateTimer.setSingleShot(true);
     d_ptr->m_updateItemsTimer.setSingleShot(true);
     d_ptr->m_fetchCollectionsTimer.setSingleShot(true);
@@ -227,6 +222,13 @@ QDeclarativeOrganizerModel::~QDeclarativeOrganizerModel()
 {
 }
 
+QHash<int, QByteArray> QDeclarativeOrganizerModel::roleNames() const
+{
+    QHash<int, QByteArray> roleNames = QAbstractItemModel::roleNames();
+    roleNames.insert(OrganizerItemRole, "item");
+    return roleNames;
+}
+
 /*!
   \qmlproperty string OrganizerModel::manager
 
diff --git a/src/imports/organizer/qdeclarativeorganizermodel_p.h b/src/imports/organizer/qdeclarativeorganizermodel_p.h
index 18eba747..d04a6357 100644
--- a/src/imports/organizer/qdeclarativeorganizermodel_p.h
+++ b/src/imports/organizer/qdeclarativeorganizermodel_p.h
@@ -108,6 +108,8 @@ public:
     explicit QDeclarativeOrganizerModel(QOrganizerManager* manager, const QDateTime& start, const QDateTime& end, QObject *parent = nullptr);
     ~QDeclarativeOrganizerModel();
 
+    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
+
     QString error() const;
     int itemCount() const;
 
-- 
2.30.2

