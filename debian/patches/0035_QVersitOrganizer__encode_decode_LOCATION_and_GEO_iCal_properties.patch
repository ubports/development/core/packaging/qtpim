Description: QVersitOrganizer: encode/decode LOCATION and GEO iCal properties
Author: Lionel Duboeuf <lduboeuf@ouvaton.org>

---
--- a/src/versitorganizer/qversitorganizerdefs_p.h
+++ b/src/versitorganizer/qversitorganizerdefs_p.h
@@ -73,7 +73,8 @@
     {"SUMMARY", QOrganizerItemDetail::TypeDisplayLabel, QOrganizerItemDisplayLabel::FieldLabel},
     {"DESCRIPTION", QOrganizerItemDetail::TypeDescription, QOrganizerItemDescription::FieldDescription},
     {"UID", QOrganizerItemDetail::TypeGuid, QOrganizerItemGuid::FieldGuid},
-    {"CATEGORIES", QOrganizerItemDetail::TypeTag, QOrganizerItemTag::FieldTag}
+    {"CATEGORIES", QOrganizerItemDetail::TypeTag, QOrganizerItemTag::FieldTag},
+    {"LOCATION", QOrganizerItemDetail::TypeLocation, QOrganizerItemLocation::FieldLabel}
 };
 
 QT_END_NAMESPACE_VERSITORGANIZER
--- a/src/versitorganizer/qversitorganizerexporter_p.cpp
+++ b/src/versitorganizer/qversitorganizerexporter_p.cpp
@@ -164,6 +164,8 @@
         encodeVisualReminder(item, detail, &generatedProperties, &processedFields);
     } else if (detail.type() == QOrganizerItemDetail::TypeEmailReminder) {
         encodeEmailReminder(item, detail, &generatedProperties, &processedFields);
+    } else if (detail.type() == QOrganizerItemDetail::TypeLocation) {
+        encodeLocation(detail, &generatedProperties, &processedFields);
     } else if (mPropertyMappings.contains(detail.type())) {
         encodeSimpleProperty(detail, *document, &removedProperties, &generatedProperties, &processedFields);
     }
@@ -697,6 +699,30 @@
     *generatedProperties << property;
 }
 
+void QVersitOrganizerExporterPrivate::encodeLocation(const QOrganizerItemDetail &detail, QList<QVersitProperty> *generatedProperties, QSet<int> *processedFields)
+{
+    const QOrganizerItemLocation &location = static_cast<const QOrganizerItemLocation &>(detail);
+
+    QVersitProperty propertyLoc;
+    propertyLoc.setName(QStringLiteral("LOCATION"));
+    propertyLoc.setValue(location.label());
+
+    *processedFields << QOrganizerItemLocation::FieldLabel;
+    *generatedProperties << propertyLoc;
+
+    if (location.latitude() != 0 && location.longitude() != 0) {
+        QVersitProperty propertyGeo;
+        propertyGeo.setName(QStringLiteral("GEO"));
+        QString value = QString::number(location.latitude(), 'f', 6);
+        value.append(QLatin1Char(';'));
+        value.append(QString::number(location.longitude(), 'f', 6));
+        propertyGeo.setValue(value);
+
+        *processedFields << QOrganizerItemLocation::FieldLatitude << QOrganizerItemLocation::FieldLongitude;
+        *generatedProperties << propertyGeo;
+    }
+}
+
 QVersitDocument QVersitOrganizerExporterPrivate::encodeItemReminderCommonFields(
         const QOrganizerItem &item,
         const QOrganizerItemReminder &reminder,
--- a/src/versitorganizer/qversitorganizerexporter_p.h
+++ b/src/versitorganizer/qversitorganizerexporter_p.h
@@ -166,6 +166,11 @@
             const QOrganizerItemDetail& detail,
             QList<QVersitProperty>* generatedProperties,
             QSet<int>* processedFields);
+    void encodeLocation(
+        const QOrganizerItemDetail &detail,
+        QList<QVersitProperty> *generatedProperties,
+        QSet<int> *processedFields
+        );
     void encodeAudibleReminder(
             const QOrganizerItem &item,
             const QOrganizerItemDetail &detail,
--- a/src/versitorganizer/qversitorganizerimporter_p.cpp
+++ b/src/versitorganizer/qversitorganizerimporter_p.cpp
@@ -175,6 +175,8 @@
             success = createRecurrenceDates(property, item, &updatedDetails);
         } else if (property.name() == QStringLiteral("RECURRENCE-ID")) {
             success = createRecurrenceId(property, item, &updatedDetails);
+        } else if (property.name() == QStringLiteral("GEO")) {
+            success = createGeoLocation(property, item, &updatedDetails);
         }
     } else if (document.componentType() == QStringLiteral("VTODO")) {
         if (property.name() == QStringLiteral("DTSTART")) {
@@ -195,6 +197,8 @@
             success = createFinishedDateTime(property, item, &updatedDetails);
         } else if (property.name() == QStringLiteral("RECURRENCE-ID")) {
             success = createRecurrenceId(property, item, &updatedDetails);
+        } else if (property.name() == QStringLiteral("GEO")) {
+            success = createGeoLocation(property, item, &updatedDetails);
         }
     } else if (document.componentType() == QStringLiteral("VALARM")) {
             success = createItemReminder(document, item, &updatedDetails);
@@ -329,6 +333,33 @@
     return true;
 }
 
+bool QVersitOrganizerImporterPrivate::createGeoLocation(const QVersitProperty &property, QOrganizerItem* item, QList<QOrganizerItemDetail> *updatedDetails)
+{
+    if (property.value().isEmpty())
+        return false;
+
+    QStringList parts = property.value().split(QLatin1Char(';'));
+    if (parts.size() < 2)
+        return false;
+
+    QOrganizerItemLocation location(item->detail(QOrganizerItemDetail::TypeLocation));
+
+    bool ok;
+    QString sLat = parts.at(0);
+    double latitude = sLat.toDouble(&ok);
+    if (ok)
+        location.setLatitude(latitude);
+
+    QString sLong = parts.at(1);
+    double longitude = sLong.toDouble(&ok);
+    if (ok)
+        location.setLongitude(longitude);
+
+    updatedDetails->append(location);
+
+    return true;
+}
+
 bool QVersitOrganizerImporterPrivate::createItemReminder(
         const QVersitDocument& valarmDocument,
         QOrganizerItem* item,
--- a/src/versitorganizer/qversitorganizerimporter_p.h
+++ b/src/versitorganizer/qversitorganizerimporter_p.h
@@ -160,6 +160,9 @@
     bool createComment(
             const QVersitProperty& property,
             QList<QOrganizerItemDetail>* updatedDetails);
+    bool createGeoLocation(
+        const QVersitProperty& property, QOrganizerItem* item,
+        QList<QOrganizerItemDetail>* updatedDetails);
     bool createItemReminder(
             const QVersitDocument &valarmDocument,
             QOrganizerItem *item,
--- a/tests/auto/versitorganizer/qversitorganizerexporter/tst_qversitorganizerexporter.cpp
+++ b/tests/auto/versitorganizer/qversitorganizerexporter/tst_qversitorganizerexporter.cpp
@@ -655,6 +655,31 @@
     }
 
     {
+        QVersitProperty property;
+        property.setName(QStringLiteral("LOCATION"));
+        property.setValue(QStringLiteral("Conference Room - F123, Bldg. 002"));
+
+        QOrganizerItemLocation location;
+        location.setLabel("Conference Room - F123, Bldg. 002");
+        QTest::newRow("location") << (QList<QOrganizerItemDetail>() << location)
+                                  << (QList<QVersitProperty>() << property);
+    }
+
+    {
+        QVersitProperty property;
+        property.setName(QStringLiteral("GEO"));
+        property.setValue(QStringLiteral("71.702379;-42.177175"));
+
+        QOrganizerItemLocation location;
+        location.setLatitude(71.702379);
+        location.setLongitude(-42.177175);
+
+        QTest::newRow("geo location")
+            << (QList<QOrganizerItemDetail>() << location)
+            << (QList<QVersitProperty>() << property);
+    }
+
+    {
         QVersitProperty property1;
         property1.setName(QStringLiteral("COMMENT"));
         property1.setValue(QStringLiteral("Comment 1"));
--- a/tests/auto/versitorganizer/qversitorganizerimporter/tst_qversitorganizerimporter.cpp
+++ b/tests/auto/versitorganizer/qversitorganizerimporter/tst_qversitorganizerimporter.cpp
@@ -75,6 +75,9 @@
         property.setName(QStringLiteral("SUMMARY"));
         property.setValue(QStringLiteral("Bastille Day Party"));
         nested.addProperty(property);
+        property.setName(QStringLiteral("LOCATION"));
+        property.setValue(QStringLiteral("Conference Room - F123, Bldg. 002"));
+        nested.addProperty(property);
         property.setName(QStringLiteral("DTSTART"));
         property.setValue(QStringLiteral("19970714T170000Z"));
         nested.addProperty(property);
@@ -112,6 +115,7 @@
         event.setDisplayLabel(QStringLiteral("Bastille Day Party"));
         event.setStartDateTime(QDateTime(QDate(1997, 7, 14), QTime(17, 0, 0), Qt::UTC));
         event.setEndDateTime(QDateTime(QDate(1997, 7, 15), QTime(3, 59, 59), Qt::UTC));
+        event.setLocation("Conference Room - F123, Bldg. 002");
 
         QOrganizerTodo todo;
         todo.setDisplayLabel(QStringLiteral("Take out the garbage"));
@@ -754,6 +758,33 @@
     }
 
     {
+        QVersitProperty property;
+        property.setName(QStringLiteral("LOCATION"));
+        property.setValue(QStringLiteral("Conference Room - F123, Bldg. 002"));
+
+        QOrganizerItemLocation location;
+        location.setLabel("Conference Room - F123, Bldg. 002");
+
+        QTest::newRow("location")
+            << (QList<QVersitProperty>() << property)
+            << (QList<QOrganizerItemDetail>() << location);
+    }
+
+    {
+        QVersitProperty property;
+        property.setName(QStringLiteral("GEO"));
+        property.setValue(QStringLiteral("71.702379;-42.177175"));
+
+        QOrganizerItemLocation location;
+        location.setLatitude(71.702379);
+        location.setLongitude(-42.177175);
+
+        QTest::newRow("geo location")
+            << (QList<QVersitProperty>() << property)
+            << (QList<QOrganizerItemDetail>() << location);
+    }
+
+    {
         QVersitProperty property;
         property.setName(QStringLiteral("UID"));
         property.setValue(QStringLiteral("1234567"));
